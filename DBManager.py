#!/usr/bin/python
# by Openscop, CiziaDugue

import sqlite3

class DBManager():

    db = sqlite3.connect('rpiclientdb')
    cursor = db.cursor()

    def __init__(self):
        # self.db = sqlite3.connect('mydb')
        # self.cursor = self.db.cursor()
        self.createTables()
        self.initData()

    # Creates or opens a file called mydb with a SQLite3 DB
    def createTables(self):
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS reader(
                id INTEGER PRIMARY KEY,
                state TEXT UNIQUE,
                msg TEXT,
                badge TEXT UNIQUE,
                djangoId INTEGER DEFAULT Null,
                code TEXT DEFAULT Null
            )
        ''')
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS users(
                id INTEGER PRIMARY KEY,
                badge TEXT UNIQUE
            )
        ''')
        self.db.commit()

    def initData(self):
        # Add default state
        self.cursor.execute('''SELECT state, msg, badge, djangoId, code FROM reader WHERE id = ?''', (1,))
        state = self.cursor.fetchone()
        if not state:
            self.cursor.execute('''INSERT INTO reader(state, msg, badge, djangoId, code) VALUES ('attenteConnexion','Fab Access. Bonjour',0,0)''')
            self.db.commit()
        else:
            self.cursor.execute('''UPDATE reader SET state = ?, msg = ?, badge = ?, djangoId = ?, code = ? WHERE id = ?''', ('attenteConnexion','Fab Access. Bonjour',0,0,0,1))
            self.db.commit()

    def getState(self):
        self.cursor.execute('''SELECT state FROM reader''')
        row = self.cursor.fetchone()
        state = row[0]
        return state

    def getMessage(self):
        self.cursor.execute('''SELECT msg FROM reader''')
        row = self.cursor.fetchone()
        msg = row[0]
        return msg

    def updateState(self, state):
        self.cursor.execute('''UPDATE reader SET state = ? WHERE id = ?''', (state,1))
        self.db.commit()

    def updateBadge(self, badge):
        self.cursor.execute('''UPDATE reader SET badge = ? WHERE id = ?''', (badge,1))
        self.db.commit()

    def updateStateAndBadge(self, state, badge):
        self.cursor.execute('''UPDATE reader SET state = ?,badge = ? WHERE id = ?''', (state,badge,1))
        self.db.commit()

    def updateStateAndMessage(self, state, msg):
        self.cursor.execute('''UPDATE reader SET state = ?,msg = ? WHERE id = ?''', (state,msg,1))
        self.db.commit()


    def updateDataForAttribution(self, state, code, djangoId, msg):
        self.cursor.execute('''UPDATE reader SET state = ?,code = ?, djangoId = ?, msg = ? WHERE id = ?''', (state, code, djangoId, msg, 1))
        self.db.commit()

    def getDataForAttribution(self, action):
        self.cursor.execute('''SELECT badge,code,djangoId FROM reader WHERE id = ?''', (1,))
        row = self.cursor.fetchone()
        # print(row)
        dict = {
            "action": action,
            "badge":row[0],
            "code":row[1],
            "id":row[2],
        }
        return dict
