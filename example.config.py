#url to homepage of fabaccess
index_url = "http://127.0.0.1:8000"
#url to check if we're try to log a user or to link his account with an RFID tag
script_url = index_url + '/panel/verifscript'
# url to get a welcoming/departing message
message_url = index_url + "/message"
#url to log the user in or out
welcome_url = index_url + "/bienvenu"
#url to link his account with an RFID tag
register_url = index_url + '/panel/receptionbadge'
#url to link his account with an RFID tag
is_user_online_url = index_url + '/panel/verifyonline'
#url
logged_user_choice_panel_url = index_url + '/borneclient'
#url
user_profile_login_url = index_url + '/connexion'

#length of the RFID tag
tag_length = 10

# chosen browser to launch the app
browser_path = "/usr/bin/chromium-browser"
chromium_debug_url = 'http://localhost:9222/json'

# tts settings
language = 'fr'
storage_dir = 'messages'
player = 'mpg123'
# delay after which unused audio files are deleted
maxTimeInDays = 100
