#!/usr/bin/python
# by Openscop, CiziaDugue

import serial
from serial.tools import list_ports
from time import sleep
import requests
import json
import websocket as wsclient
import subprocess
from gtts import gTTS
import os, sys
from datetime import datetime
import re
from config import *
from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
import multiprocessing
from multiprocessing import Process, JoinableQueue, TimeoutError

global cursor, browser

#Finds the port on which is plugged the RFID reader and prints its name
port = [port[0] for port in list_ports.comports()]
print(port)

class Browser():
    def __init__(self, browser_path):
        self.browser_path = browser_path

    #Opens app mode and debug-mode browser on port 9222 with subprocess
    def openBrowser(self, url):
        args = [self.browser_path, '--remote-debugging-port=9222',  '--app=' + url]
        # args = [self.browser_path, '--remote-debugging-port=9222', '--kiosk',  '--app=' + url]
        browser = subprocess.Popen(args, shell=False)
        return browser

    # Redirects to the chosen url using chormium websocket server
    def redirect(self, chromium_debug_url, browser_queue):
        while True:
            url = browser_queue.get()
            print(url)
            if url:
                # Setup websocket connection:
                geturl = requests.get(chromium_debug_url)
                websocketURL = json.loads(geturl.content.decode('utf-8'))[0]['webSocketDebuggerUrl']
                ws = wsclient.create_connection(websocketURL)
                # Navigate to url:
                request = {}
                request['id'] = 1
                request['method'] = 'Page.navigate'
                request['params'] = {"url": url}
                print(request)
                ws.send(json.dumps(request))
                result = ws.recv()
                print("Browser - Page.navigate: ", result)
                frameId = json.loads(result)['result']['frameId']
                ws.close()


class Reader():

    def __init__(self, tag_length):
        # self.ser = serial.Serial('/dev/ttyUSB0', 2400, timeout=0.5)
        self.badge = None
        self.tag_length = tag_length

    def read(self):
        while True:
            ser = serial.Serial('/dev/ttyUSB0', 2400, timeout=0.5)
            string1 = ser.read(12)[1:-1].decode('utf-8')
            sleep(0.1)
            string2 = ser.read(12)[1:-1].decode('utf-8')
            if len(string1) < self.tag_length and len(string1) > 0:
                badge = 'erreurBadge'
                print('Reader - read : '+badge)
                sleep(1)
                return badge
            elif string1 == string2 and len(string1) > 0:
                badge = string1
                print('Reader - read : '+badge)
                sleep(1)
                return badge

    def run(self, websocket_queue, tts_queue, reader_queue, browser_queue, wssend_badge_queue, readyattribution_queue, clients):
        global badge
        while True:
            state = reader_queue.get()
            print('Reader.run -- state :' + state)
            if state == 'attenteConnexion':
                badge = self.read()
                if badge != 'erreurBadge' and badge != '':
                    userIsLoggedResponse = requests.get(is_user_online_url + "?badgetag=" + badge)
                    userIsLogged = userIsLoggedResponse.text
                    print('Reader.run -- userIsLogged : ' + userIsLogged)
                    # unlogged member
                    if userIsLogged == '0':
                        url = welcome_url + "?badgetag=" + badge
                        browser_queue.put(url)
                        print('Reader.run -- url in bq : ' + url + ' + state in rq : ' + state)
                    # logged member
                    elif userIsLogged == '1':
                        url = logged_user_choice_panel_url
                        browser_queue.put(url)
                        websocket_queue.put(badge)
                        print('Reader.run -- badge in wsq : '+badge+' + url in bq : '+url)
                    # admin
                    elif userIsLogged == '2':
                        url = welcome_url + "?badgetag=" + badge
                        browser_queue.put(url)
                        print('Reader.run -- url in bq : ' + url + ' + state in rq : ' + state)

                elif badge == 'erreurBadge':
                    msg = 'Erreur de lecture du badge'
                    tts_queue.put(msg)
                    state == 'attenteConnexion'
                    reader_queue.put(state)
                    print('Reader.run -- err msg in tq : ' + msg + ' + state in rq : ' + state)

            elif state == 'envoiBadge':
                print("envoie en cours")
                badge = self.read()
                print('fait')
                if badge != 'erreurBadge' and badge != '':
                    wssend_badge_queue.put(badge)
                    reader_queue.task_done()
                    state = 'arretLecture'
                    reader_queue.put(state)
                    readyattribution_queue.put('ok')
                    print('Reader.run -- badge in wssq : ' + badge)

                elif badge == 'erreurBadge':
                    msg = 'Erreur de lecture du badge'
                    tts_queue.put(msg)
                    state == 'envoiBadge'
                    reader_queue.put(state)
                    print('Reader.run -- err msg in tq : '+msg+' + state in rq : ' + state)

            elif state == 'arretLecture':
                print('Reader.run -- state in rq : ' + state)


class TextToSpeech():
    def __init__(self, language, storage_dir, player, maxTimeInDays):
        self.language = language
        self.storage_dir = storage_dir
        self.player = player
        self.findOrMakeStorageDir()
        self.removeUnusedFiles(maxTimeInDays)

    def findOrMakeStorageDir(self):
        if not os.path.exists(self.storage_dir):
            os.mkdir(self.storage_dir)

    def removeUnusedFiles(self, maxTimeInDays):
        now = int(datetime.utcnow().timestamp())
        maxTimeInSeconds = maxTimeInDays * 60 * 60 * 24
        limit = now - maxTimeInSeconds
        bytesDirectory = os.fsencode(self.storage_dir)
        for file in os.listdir(bytesDirectory):
            stringFileName = file.decode("utf-8")
            filepath = os.path.realpath(self.storage_dir + '/' + stringFileName)
            atime = int(os.stat(filepath).st_atime)
            if atime < limit:
                os.remove(filepath)

    def makeFileName(self, message):
        # Remove all non-word characters (everything except numbers and letters)
        file_name = re.sub(r"[^\w\s]", '', message)
        # Replace all runs of whitespace with an underscore
        file_name = re.sub(r"\s+", '_', message)
        file_name = file_name.lower()
        return file_name

    def findOrMakeAudioFile(self, message):
        if not os.path.exists(self.storage_dir + '/' + "%s.mp3" % self.file_name):
            audio = gTTS(text = message, lang = self.language, slow = False)
            audio.save("%s.mp3" % os.path.join(self.storage_dir, self.file_name))

        return os.path.realpath(self.storage_dir + '/' + "%s.mp3" % self.file_name)

    def playAudioFile(self):
        cmd = 'mpg123 ' + self.file_path
        play = subprocess.Popen(cmd, shell=True)

    def run(self, tts_queue):
        while True:
            msg = tts_queue.get()
            print(str(msg))
            if msg:
                print('TTS.run -- msg from tq : ' + msg)
                self.file_name = self.makeFileName(msg)
                self.file_path = self.findOrMakeAudioFile(msg)
                self.playAudioFile()
                msg = None
                tts_queue.task_done()

clients = []
class Communicate(WebSocket):

    def handleMessage(self):
        global websocket_queue, tts_queue, reader_queue, browser_queue, wssend_data_queue, clients, badge, readyattribution_queue
        data = json.loads(self.data)
        print('ws.handleMessage -- data recieved -- data : ' + str(data))
        sys.stdout.flush()
        action = data['action']

        if action == 'connexion' or action == 'erreur':
            msg = data['msg']
            tts_queue.put(msg)
            state = 'attenteConnexion'
            reader_queue.put(state)
            print('ws.handleMessage -- connexion received msg from tq : ' + msg+ ' state in rq : ' + state)
            sys.stdout.flush()

        elif action == 'attenteConnexion' or action == 'arretLecture':
            msg = data['msg']
            if msg:
                tts_queue.put(msg)
            reader_queue.put(action)
            print('ws.handleMessage -- action in rq : ' + action)
            print('ws.handleMessage -- connexion received msg from tq : ' + str(msg)+ ' state in rq : ' + action)

        elif action == 'choixUtilisateur':
            reader_queue.put(action)
            msg = data['msg']
            print(msg)
            print('ws.handleMessage -- action in rq : ' + action)
            tts_queue.put(msg)
            while 'choixUtilisateurEnCours':
                print('choixUtilisateurEnCours')
                badge = websocket_queue.get()
                print('ws.handleMessage -- choixUtilisateurEnCours -- badge form wsq : ' + badge)
                if badge:
                    break

        elif action == 'deconnexion':
            url = welcome_url + "?badgetag=" + badge
            browser_queue.put(url)
            print('ws.handleMessage -- url in bq : ' + url)
            sys.stdout.flush()

        elif action == 'profil':
            url = user_profile_login_url + "?badgetag=" + badge
            browser_queue.put(url)
            print('ws.handleMessage -- url in bq : ' + url)
            sys.stdout.flush()

        elif action == 'envoiBadge':
            print('ok')
            reader_queue.put(action)
            wssend_data_queue.put(data)

            msg = data['msg']
            tts_queue.put(msg)

        elif action == 'tic':
            if not readyattribution_queue.empty():
                ready = readyattribution_queue.get()
                data = wssend_data_queue.get()
                badge = wssend_badge_queue.get()
                data['badge'] = badge
                payload = json.dumps(data)
                self.sendMessage(payload)
                print('done')



    def handleConnected(self):
        global clients, clients_queue
        print(self.address, 'connected')
        sys.stdout.flush()
        clients.append(self)
        print('ws.handleConnected -- client appended : ' + str(clients))
        # sys.stdout.flush()

    def handleClose(self):
        global clients
        print(self.address, 'closed')
        sys.stdout.flush()
        clients.remove(self)

        print('ws.handleClose -- client removed : ' + str(clients))

def websocket_server(websocket_queue, tts_queue, reader_queue, browser_queue, wssend_data_queue,):
    server = SimpleWebSocketServer('', 5678, Communicate)

    server.serveforever()


# Starting processes
def init():
    global badge, websocket_queue, tts_queue, reader_queue, browser_queue, wssend_data_queue, wssend_badge_queue, clients, readyattribution_queue, clients_queue

    reader = Reader(tag_length)
    browser = Browser(browser_path)
    browser.openBrowser(index_url)
    tts = TextToSpeech(language, storage_dir, player, maxTimeInDays)

    if __name__ == '__main__':
        reader_queue = JoinableQueue()
        tts_queue = JoinableQueue()
        websocket_queue = JoinableQueue()
        browser_queue = JoinableQueue()
        wssend_data_queue = JoinableQueue()
        wssend_badge_queue = JoinableQueue()
        readyattribution_queue= JoinableQueue()
        clients_queue= JoinableQueue()

        websocket_proc = multiprocessing.Process(name='websocket_server',target=websocket_server, args=(websocket_queue, tts_queue, reader_queue, browser_queue, wssend_data_queue,))


        reader_proc = multiprocessing.Process(name='reader',target=reader.run, args=(websocket_queue, tts_queue, reader_queue, browser_queue, wssend_badge_queue,readyattribution_queue, clients_queue))

        tts_proc = multiprocessing.Process(name='tts',target=tts.run, args=(tts_queue,))

        browser_proc = multiprocessing.Process(name='browser',target=browser.redirect, args=(chromium_debug_url, browser_queue,))

        reader_proc.start()
        tts_proc.start()
        websocket_proc.start()
        browser_proc.start()

        try:
            msg = 'Fab Access. Bonjour'
            state = 'attenteConnexion'
            tts_queue.put(msg)
            reader_queue.put(state)
            print('init msg in tq : ' + msg + ' state in rq : ' + state)
            sleep(1)
        except TimeoutError:
            pass
        finally:
            websocket_proc.join()
            reader_proc.join()
            tts_proc.join()
            browser_proc.join()

# Runs Program
init()
