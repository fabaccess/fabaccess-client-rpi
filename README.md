# FabAccess - Client RaspberryPi

## Contexte

Afin de faciliter la gestion d'un FabLab, nous proposons un système d'identification par badge RFID.
Ce client RaspberryPi dialogue via un server websocket avec une application django de gestion des utilisateurs et des machines du FabLab. Il envoie à l'application l'identifiant des cartes scannées pour qu'elles soient attribuées par les FabManager ou pour que les utilisateurs enregistrés signalent leur présence au FabLab.

----
## Fonctionnalités

- Lire de cartes RFID
- Gérer de la navigation avec Chromium debugtools (démarrage et redirections)
- Synthétiser et la diffuser des messages audio personnalisés pour guider l'utilisateur
- Dialoguer avec l'application FabAccess par requêtes HTTP pour les identifiants et via un serveur websocket pour les messages en synthèse vocale

----
## Prérequis

- RaspberryPi (testé sur Raspian 9 Stretch)
- Lecteur de carte RFID
- Cartes RFID
- Navigateur Chromium-Browser

----
## Environnement

### Python 3
Fourni par défaut avec Raspbian  
`$ python3 --version` `$ sudo apt install python3-dev`  
<https://www.python.org/>

### pip
Fourni avec python3-dev  
`$ sudo apt install python3-pip`  
<https://pypi.org/project/pip/>

### Modules python

#### Environnement virtuel python : virtualenv
`$ pip3 install virtualenv`  
<https://virtualenv.pypa.io/en/latest/>

#### Gestion des requêtes HTTP : requests
`$ pip install requests`  
<https://2.python-requests.org/en/master/>

#### Gestion de processus fils : subprocess
`$ pip install subprocess`  
<https://docs.python.org/fr/3/library/subprocess.html>

#### Pilotage du port série : pyserial
`$ pip install pyserial`  
<https://pythonhosted.org/pyserial/>

#### Synthèse vocale : gTTS
`$ pip install gTTS`  
<https://gtts.readthedocs.io/en/latest/index.html>

#### Lecture audio : pygame
`$ python3 -m pip install pygame`  
<https://www.pygame.org/news>

----
## Installer un clavier virtuel

Rechercher un plugin de clavier virtuel pour Chrome.
Par exemple :
<https://chrome.google.com/webstore/detail/virtual-keyboard/pflmllfnnabikmfkkaddkoolinlfninn?hl=en>

Configurez le plugin pour que le clavier se lance automatiquement pour le site Fabaccess.
Dans Chromium :
- Menu > More tools > Extensions
- Cliquer sur Détails du plugin installé
- Configurer avec l'url de FabAccess pour que le clavier s'ouvre automatiquement pendant la navigation

----
## Installation du client

Créer et activer un environnement virtuel
- `$ virtualenv -p python3 environment_name`
- `$ cd environment_name`
- `$ source bin/activate`

Installation des modules
- `$ pip install -r requirements.txt`
- Ou, pour installer les modules un à un : `$ pip install package_name`
- `$ python3 -m pip install pygame`  

Clonage du dépôt
- `$ git clone https://gitlab.com/fabaccess/fabaccess-client-rpi.git`

Création du fichier config.py
- `$ cd project_folder`
- `$ cp example.config.py config.py`
- Compléter le fichier créé avec l'url de votre application Fabaccess django

----
## Exécution du programme

Lancer les scripts dans deux terminaux distincts (avec virtualenv activé):
- `$ python websocket_server.py`
- `$ python main.py`
