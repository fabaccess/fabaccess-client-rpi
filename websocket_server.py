#!/usr/bin/python
# by Openscop, CiziaDugue

import sqlite3
import json
from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
from time import sleep
# from threading import Thread

#Loads config from config.py
from config import *
from DBManager import DBManager

global DB
# global cursor, db

class Communicate(WebSocket):

    def handleMessage(self):
        # global cursor, db
        global DB
        data = json.loads(self.data)
        action = data['action']
        print(action)


        if action != 'envoiBadge':
            state = action
            msg = data['msg']
            print(msg)
            DB.updateStateAndMessage(state,msg)
            print('state :' + state + '--- action :' + action)
            sleep(2)


        elif action == 'envoiBadge':
            print(action + ' demandé')
            state = action
            code = data['code']
            djangoId = data['id']
            msg = data['msg']
            print(msg)
            badge = None
            DB.updateDataForAttribution(state, code, djangoId, msg)
            print('waiting for badge -----' + action)
            sleep(2)
            while "Le badge n'est pas encore lu":
                state = DB.getState()
                # action = data['action']
                print('state :' + state + '--- action :' + action)
                sleep(1)
                if state == 'pretPourAttribution':
                    dictpayload = DB.getDataForAttribution(action)
                    payload = json.dumps(dictpayload)
                    print(payload)
                    self.sendMessage(payload)
                    break

    def handleConnected(self):
        print(self.address, 'connected')

    def handleClose(self):
        print(self.address, 'closed')

# Sqlite DB config and initdata
DB = DBManager()
# Starts websocket server
server = SimpleWebSocketServer('', 5678, Communicate)
server.serveforever()

# while True:
#     sendMsg = SimpleMessages().errorMessage()
